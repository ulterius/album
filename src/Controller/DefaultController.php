<?php /**
 * @file
 * Contains \Drupal\album\Controller\DefaultController.
 */

namespace Drupal\album\Controller;

use Drupal\Core\Controller\ControllerBase;
//use Drupal\Core\File\FileSystem;
//use Drupal\Core\Site\Settings;
use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\Core\Image;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Render\Markup;
use Drupal\album\AlbumGallery;
use Drupal\album\AlbumImageClass;
/**
 * Default controller for the album module.
 */
class DefaultController extends ControllerBase {

  public function album() {
	 \Drupal::service('page_cache_kill_switch')->trigger();

	$out=array();
   	$path=realpath(DRUPAL_ROOT . '/' . PublicStream::basePath()).'/'.\Drupal::config('album.settings')->get('album_directory');

    	if (!file_exists($path)) {
      		if (!mkdir($path, 0755)) {
        		\Drupal::messenger()->addMessage($path . t(" Cannot be created!"), $error);
      		}
    	}
    	$user = \Drupal::currentUser();
	$out[] = [ '#markup' => Markup::create((new  AlbumGallery)->showalbums($path))];
   	if (\Drupal::currentUser()->hasPermission('admin album') || $user->uid == '1') {
		$out[] = \Drupal::formBuilder()->getForm('\Drupal\album\Form\FormNewAlbum',$path);
    	}
	
	$out[]= [
        	'#type' => 'markup',
        	'#markup' => '',
      		'#attached' => [
        		'library' => [
          		'album/album', 
        		]
      		]
    	];
	return $out;
  }


  public function showalbum($albumid) {
	 \Drupal::service('page_cache_kill_switch')->trigger();
	$out=array();
	$result=(new  AlbumGallery)->album_showalbum($albumid);
	if (!$result) {
		$out[] = \Drupal::formBuilder()->getForm('\Drupal\album\Form\FormGetPasswordAlbum',$albumid);
	}else{
	  	if (\Drupal::currentUser()->hasPermission('admin album') || $user->uid=='1') {
			$out[] = \Drupal::formBuilder()->getForm('\Drupal\album\Form\FormAdminAlbum',$albumid);
	  	}
		$out[]=[ '#markup' =>Markup::create($result)];
	}
	$out[]= [
        	'#type' => 'markup',
        	'#markup' => '',
      		'#attached' => [
        		'library' => [
          		'album/album', 
        		]
      		]
    	];
	return $out;
  }

  public function alter_album($id) {
    // @FIXME
// // @FIXME
// // This looks like another module's variable. You'll need to rewrite this call
// // to ensure that it uses the correct configuration object.
// $path=variable_get('file_public_path', conf_path() . '/files')."/" . variable_get('album_directory','album');

		$out[] = \Drupal::formBuilder()->getForm('\Drupal\album\Form\FormAlterAlbum',$id);
    /*
	$out.="<hr>";
	$out.= drupal_render(drupal_get_form('form_password_album',$id));
	$out.="<hr>";
	$out.=l(t('Delete this album'),"album/deletealbum/".$id);
*/
	$out[]= [
        	'#type' => 'markup',
        	'#markup' => '',
      		'#attached' => [
        		'library' => [
          		'album/album', 
        		]
      		]
    	];
    return($out);
  }

  public function altervideo_album($albumid, $id) {
	 \Drupal::service('page_cache_kill_switch')->trigger();
    $out .= \Drupal::service("renderer")->render(\Drupal::formBuilder()->getForm('form_album_video', $albumid, $id));
    $out .= "<br><hr><br>";
    // @FIXME
    // l() expects a Url object, created from a route name or external URI.
    // $out.=l(t('Delete this video'),"album/deletevideo/".$id);

    return($out);
  }


 public function album_photo_list($albumid=''){
	 \Drupal::service('page_cache_kill_switch')->trigger();
     $header = array(
        array(''),
        array('data' => t('Photo'), 'field' => 'a.photo'),
        array('data' => t('Title'), 'field' => 'a.title'),
        array('data' => t('Description'), 'field' => 'a.description'),
        array('data' => t('Slide show'), 'field' => 'a.random'),
        array('data' => t('Album photo'), 'field' => 'a.albumphoto'),
      );

	$con = \Drupal\Core\Database\Database::getConnection();
    	$query = $con->select('albumphoto', 'a')->extend('Drupal\Core\Database\Query\TableSortExtender');
	$query->fields('a', ['id','photo','title','description','random','albumphoto', 'albumid']);
	if(!empty($albumid)){
		$query = $query ->condition('a.albumid', $albumid);
	}else{
		$query = $query ->condition('a.random', '1');
	}

    $result = $query
      ->orderByHeader($header)
      ->execute();

    $rows = [];
    foreach ($result as $row) {
	if(empty($albumid)){
		$id=$row->albumid;
	}else{
		$id=$albumid;
	}	
	$fields=[];
	if (\Drupal::currentUser()->hasPermission('admin album') || $user->uid == '1') {
                        $link="/album/alter/".$row->id;
                        $img="<img src=/".\Drupal::service('extension.list.module')->getPath('album')."/images/edit.gif>";
                        $out=(new AlbumImageClass)->album_image_link($link,$img);
                        $link="/album/delete/".$row->id;
                        $img="<img src=/".\Drupal::service('extension.list.module')->getPath('album')."/images/delete.gif>";
                        $out.=(new AlbumImageClass)->album_image_link($link,$img);
                }

	$fields[]=Markup::create($out);
	$fields[]=Markup::create((new AlbumImageClass)->album_get_thumb($id,$row->photo));
	$fields[]=$row->title;
	$fields[]=$row->description;
	$fields[]=($row->random == '1') ? t('Yes') : t('No');
	$fields[]=($row->albumphoto == '1') ? t('Yes') : t('No');
	$rows[] = $fields;
    }

    $build['tablesort_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

    return $build;
  }

}
