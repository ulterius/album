<?php
/**
 * @file
 * Contains \Drupal\album\AlbumFilesClass.
 */
use Drupal\file\Entity\File;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\FileInterface;
use Drupal\file\FileStorageInterface;
use Drupal\Core\Image\ImageFactory;

namespace Drupal\album;

/**
 * Files manupulation
 *
 */
class AlbumFilesClass {
  public function handleFileDelete($uri) {
//	  $uri="public://photos/test2/upload/IMG_20200710_170901.jpg";
	  $row=\Drupal::database()->query("select fid from file_managed where uri= :uri" ,array(':uri' => $uri))->fetchObject();
	  if(!empty($row->fid)){
	  	$fid=$row->fid;
	  	$file = \Drupal\file\Entity\File::load($row->fid);
    	  	$file->delete();
	  }else{
		  \Drupal::service("file_system")->delete($uri);
	  }
  }

}

