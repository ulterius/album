<?php
/**
 * @file
 * Contains \Drupal\album\AlbumGallery.
use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\Core\Image;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\file\Entity\File;
use Drupal\Core\File\FileSystemInterface;
//use Drupal\album\AlbumImageClass;
use Symfony\Component\HttpFoundation\RedirectResponse;
 */


namespace Drupal\album;

/**
 *The galleries
 *
 */
class AlbumGallery extends AlbumImageClass{

public function showalbums($path,$parent=''){
	\Drupal::service('page_cache_kill_switch')->trigger();
	$user = \Drupal::currentUser();
	if(\Drupal::config('album.settings')->get('album_autoscan')){
		$file_system = \Drupal::service('file_system');
		$albumdir=$file_system->scanDirectory($path, '/.*$/', array('recurse' => FALSE));
		foreach ($albumdir as $dir) {	
			$album_photo = '';
			$album_dirname = $path."/".$dir->filename;
			if (is_dir($album_dirname)) {
				$row= \Drupal::database()->query("select id,title from album where album= :album" ,array(':album' => $dir->filename))->fetchObject();
				if(!$row){
					$entry['album']=$dir->filename;
					$entry['title']= str_replace("_", " ", $dir->filename);
					try {
						$id = \Drupal::database()->insert('album')->fields($entry)->execute();
                               			}
                       			catch (Exception $e) {
                               			\Drupal::messenger()->addMessage(t('\Drupal::database()->insert failed. Message = %message, query= %query',
                               			array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
                       			}
				}
			}
		}
	}
	$out.=  '<div class="album_grid-container">';
        $query = \Drupal::database()->select('album', 'a');
        $query->addField('a','album');
        $query->addField('a','title');
        $query->addField('a','description');
        $query->addField('a','password');
        $query->addField('a','id');
	$query->orderBy('a.album', 'ASC');
        $result = $query->execute();
	if(!empty($result)){
        foreach($result as $row){
		$thumb='';
		if(!empty($row->password)){
				$thumb="<img src=\"/".\Drupal::service('extension.list.module')->getPath('album')."/images/password_required.png\">";
		}else{
			$trow= \Drupal::database()->query("select photo from albumphoto where albumid= :albumid and albumphoto= TRUE" ,array(':albumid' => $row->id))->fetchObject();
			if($trow){
				$album_thumb = $trow->photo;
			}else{
				$trow= \Drupal::database()->query("select photo from albumphoto where albumid= :albumid order by rand() limit 1" ,array(':albumid' => $row->id))->fetchObject();
				$album_thumb = $trow->photo;
			}
			$thumb=(new AlbumImageClass)->album_get_thumb($row->id, $album_thumb);
		}

		$out.= '<div class="album_grid-item">';
		$link = "/album/show/".$row->id;
		$out.= '<div class="album_items" >';
		$out.=(new AlbumImageClass)->album_image_link($link,$thumb);
		$out.= "<div class=\"album_desc\">";
  		if (\Drupal::currentUser()->hasPermission('admin album') || $user->uid=='1') {
			$link="/album/alteralbum/".$row->id;
			$img="<img src=/".\Drupal::service('extension.list.module')->getPath('album')."/images/edit.gif>";
			$out.=(new AlbumImageClass)->album_image_link($link,$img);
			$link="/album/deletealbum/".$row->id;
			$img="<img src=/".\Drupal::service('extension.list.module')->getPath('album')."/images/delete.gif>";
			$out.=(new AlbumImageClass)->album_image_link($link,$img);
			if(!empty($row->password)){
				$link="/album/password/".$row->id;
				$img="<img src=/".\Drupal::service('extension.list.module')->getPath('album')."/images/key.gif>";
				$out.=(new AlbumImageClass)->album_image_link($link,$img);
			}
		}
		$out.=(new AlbumImageClass)->album_link('/album/show/'.$row->id,$row->title);
		if(!empty($row->description)){
			$out.="<br>\n";
			$out.= $row->desc;
		}
		$out.= "</div>\n";
		$out.= "</div>\n";
		$out.= "</div>\n";
	}
	$out.= "</div>\n";
      	if (\Drupal::currentUser()->hasPermission('admin album') || $user->uid == '1') {
		$out.= "<hr>\n";
		$out.=(new AlbumImageClass)->album_link('/album/list','List slide show photos');
	}
	}else{
			$out=t('No albums found');
	}

	return $out;
}
public function album_showalbum($albumid){
	\Drupal::service('page_cache_kill_switch')->trigger();
      	$row = \Drupal::database()->query("select password from album where id= :albumid", [ ':albumid' => $albumid, ])->fetchObject();
	if (!empty($row->password)){
		$password=trim($row->password);
		$cookie=trim($_COOKIE['Drupal_visitor_album'.$albumid]);
//		$cookie=htmlspecialchars($_COOKIE['Drupal_visitor_album'.$albumid]);
		if($password != $cookie){
			return FALSE;
		}
	}
	if(\Drupal::config('album.settings')->get('album_autoscan')){
		//Scan for uploaded images	
		(new AlbumImageClass)->MoveImagesUpload($albumid);
		//Scan for photo's
    		$photo_array = array();
		$pattern =(new AlbumImageClass)->album_photo_pattern();
    		$optionimg = ['attributes' => ['class' => '']];
    		$albumpath = (new AlbumImageClass)->album_get_path($albumid);
		$file_system = \Drupal::service('file_system');
    		$files=$file_system->scanDirectory($albumpath['path']."/thumbs/", $pattern, [ 'recurse' => FALSE ]);
		foreach ($files as $file) {	
      			$row = \Drupal::database()->query("select id from albumphoto where photo= :photo and albumid= :albumid", [ ':photo' => $file->filename, ':albumid' => $albumid, ])->fetchObject();
      			if (!$row) {
        			if (!empty($albumid)) {
          					$entry['albumid'] = $albumid;
        			} else {
          				$entry['albumid'] = 0;
        			}
        			$entry['photo'] = $file->filename;
        			$entry['title'] = $file->filename;
        			try {
          				$id = \Drupal::database()->insert('albumphoto')
            				->fields($entry)
            				->execute();
        			}
        	
          			catch (Exception $e) {
          				\Drupal::messenger()->addMessage(t('\Drupal::database()->insert failed. Message = %message, query= %query', [
            				'%message' => $e->getMessage(),
            				'%query' => $e->query_string,
          				]), 'error');
        			}
      			}
		}
	}

	//Show photo's
    	$out.= "<div class=\"album_title\">";
    	$out.= str_replace("_", " ", $albumpath['album']);
    	if (\Drupal::currentUser()->hasPermission('admin album') || $user->uid == '1') {
		$link="/album/alteralbum/".$albumid;
		$img="<img src=/".\Drupal::service('extension.list.module')->getPath('album')."/images/edit.gif>";
		$out.=(new AlbumImageClass)->album_image_link($link,$img);
    	}

    	$out.= "</div>";
	$out.=  '<div class="album_grid-container">';

        $query = \Drupal::database()->select('albumphoto', 'a');
        $query->addField('a','photo');
        $query->addField('a','title');
        $query->addField('a','description');
        $query->addField('a','id');
      	$query = $query ->condition('a.albumid', $albumid);
	$query->orderBy('a.photo', rand());
        $result = $query->execute();
        foreach($result as $row){
		$out.= '<div class="album_grid-item">';
		$out.= '<div class="album_item">';
      		$options = [
        		'attributes' => [
          		'class' => 'colorbox cboxElement',
          		'data-colorbox-gallery' => 'gallery-beforeafter',
          		'title' => $row->description,
          		'alt' => $row->title,
        		],
      		];
		$img=(new AlbumImageClass)->album_get_thumb($albumid,$row->photo);
      		$link = (new AlbumImageClass)->album_get_large($albumid,$row->photo);
		$out.=  (new AlbumImageClass)->album_image_link($link,$img,$options);
      		$out.=  "<div class=\"album_desc\">";
      		$out.=  $row->title;
      		if (\Drupal::currentUser()->hasPermission('admin album') || $user->uid == '1') {
			$link="/album/alter/".$row->id;
			$img="<img src=/".\Drupal::service('extension.list.module')->getPath('album')."/images/edit.gif>";
			$out.=(new AlbumImageClass)->album_image_link($link,$img);
			$link="/album/delete/".$row->id;
			$img="<img src=/".\Drupal::service('extension.list.module')->getPath('album')."/images/delete.gif>";
			$out.=(new AlbumImageClass)->album_image_link($link,$img);
      		}
      		$out.=  "</div>\n</div>\n";
		$out.=  "</div>\n";
    	}
	$out.= '</div>';
	$out.=  (new AlbumGallery)->showalbum_video($albumid);
	$out.=  (new AlbumGallery)->album_youtube($albumid);
	return $out;
}
public function album_youtube($albumid){
        $optionimg = array( 'html' => TRUE, 'attributes' => array( 'class' => '',));
        $query = \Drupal::database()->select('albumembeddedvideo', 'p');
        $query->addField('p','videoid');
        $query->addField('p','title');
        $query->addField('p','description');
        $query->addField('p','id');
        $query->addField('p','albumid');
        $query = $query ->condition('p.albumid', $albumid);
        $result = $query->execute();

        foreach($result as $row){
                $desc=$row->description;
                $title=$row->title;
		$out.= '<div class="album_videoitem">';
		$out.=str_replace('xxxxxxxxxx',$row->videoid,\Drupal::config('album.settings')->get('album_youtube_url'));
		$out.= "<div class=\"album_desc\">";
		if( !empty($row->title)){
			$out.= "<h3>".$row->title."</h3>";
		}
		if( !empty($row->description)){
			$out.= $row->description."<br>";
		}
      		if (\Drupal::currentUser()->hasPermission('admin album') || $user->uid == '1') {
                        $link="/album/imbedded-video/".$row->albumid."/".$row->id;
                        $img="<img src=/".\Drupal::service('extension.list.module')->getPath('album')."/images/edit.gif>";
			$out.=(new AlbumImageClass)->album_image_link($link,$img);
                        $link="/album/delete-imbedded-video/".$row->id;
                        $img="<img src=/".\Drupal::service('extension.list.module')->getPath('album')."/images/delete.gif>";
			$out.=(new AlbumImageClass)->album_image_link($link,$img);
                }
                $out.= "</div>";
                $out.= "</div>\n";
        }
        return $out;
}



public function showalbum_video($albumid){
	$photo_array = array();
	$albumbaseuri=(new AlbumImageClass)->album_get_baseuri($albumid);
	$pattern=(new AlbumImageClass)->album_video_pattern();
	if(\Drupal::config('album.settings')->get('album_autoscan')){
		$albumpath=(new AlbumImageClass)->album_get_path($albumid);
		$file_system = \Drupal::service('file_system');
    		$files=$file_system->scanDirectory($albumpath['path'], $pattern, [ 'recurse' => FALSE ]);
		foreach ($files as $file) {	
			$row=\Drupal::database()->query("select id from albumvideo where video= :video and albumid= :albumid" ,array(':video' => $file->filename, ':albumid' => $albumid))->fetchObject();
			if(!$row){
				if(!empty($albumid)){
					$entry['albumid']=$albumid;
				}else{
					$entry['albumid']=0;
				}
				$entry['video']=$file->filename;
				try {
               				$id = \Drupal::database()->insert('albumvideo')
                      				->fields($entry)
                       				->execute();
               			}
               			catch (Exception $e) {
               				\Drupal::messenger()->addMessage(t('\Drupal::database()->insert failed. Message = %message, query= %query',
               				array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
               			}
			}
		}
	}
        $query = \Drupal::database()->select('albumvideo', 'p');
        $query->addField('p','video');
        $query->addField('p','title');
        $query->addField('p','description');
        $query->addField('p','id');
        $query->addField('p','albumid');
        $query = $query ->condition('p.albumid', $albumid);
        $result = $query->execute();
        foreach($result as $row){
		$mime_content_type= mime_content_type($albumpath['path']."/".$row->video);
		$out.= '<div class="album_videoitem">';

                $video= '<video controls width="'.\Drupal::config('album.settings')->get('album_video_size').'" autobuffer controls >';
                $video.= '<source src="'.$albumbaseuri.'/'.$row->video.'" type="'.$mime_content_type.'"></source>';
                $video.= 'This browser does not support HTML5';
		$video.='</video>';
		$out.=$video;
		$out.= "<div class=\"album_desc\">";
		if( !empty($row->title)){
			$out.= "<h3>".$row->title."</h3>";
		}
		if( !empty($row->description)){
			$out.= $row->description."<br>";
		}
                if (\Drupal::currentUser()->hasPermission('admin album') || $user->uid=='1') {
                        $link="/album/video/".$albumid."/".$row->id;
                        $img="<img src=/".\Drupal::service('extension.list.module')->getPath('album')."/images/edit.gif>";
			$out.=(new AlbumImageClass)->album_image_link($link,$img);
                        $link="/album/delete/video/".$row->id;
                        $img="<img src=/".\Drupal::service('extension.list.module')->getPath('album')."/images/delete.gif>";
			$out.=(new AlbumImageClass)->album_image_link($link,$img);
                        $out.= "<br>\n";
                }
                $out.='<br>'.$row->video;
		$out.='</div>';
		$out.='</div>';
        }
        return $out;
}


public function sanitize_name($name) {
  $special_chars = array("?","[","]","(",")","/","\\","=","+","<",">",":",";",",","'","*","#","$","%","^","&");
  $name = str_replace($special_chars,"",$name);
  $name = str_replace(' ','_',$name);
  $name = strtolower($name);
  return ($name);
}

public function sanitize_albumname($name) {
  $special_chars = array("?","[","]","(",")","/","\\","=","+","<",">",":",";",",","'","*","#","$","%","^","&");
  $name = str_replace($special_chars,"",$name);
  $name = str_replace(' ','_',$name);
  return ($name);
}

}
