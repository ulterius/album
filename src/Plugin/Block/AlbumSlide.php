<?php

namespace Drupal\album\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\album\AlbumImageClass;


/**
 * Provides 'ALbumBLock'.
 * @Block(
 *  id = "album_block",
 *  admin_label = @Translation("Album BLock"),
 * )
*/
class AlbumSlide extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
        $block_content = '';
        $content = '';
        $photos=array();
        $result = \Drupal::database()->query("select albumid,photo FROM albumphoto WHERE random='1' ORDER BY RAND()");
        foreach ($result as $row) {
                if($row){
			$large=(new AlbumImageClass)->album_get_large($row->albumid,$row->photo);
                        $content.="<li>"."<img src=\"".$large."\" class=\"imgslide\" ></li>\n";
                }
        }
        $out[] = [ '#markup' => "<ul class=\"slider\">".$content."</ul>\n"];
        $out[]= [
                '#type' => 'markup',
                '#markup' => '',
                '#attached' => [
                        'library' => [
                        'album/album',
                        ]
                ]
        ];
        return $out;

     
  }
  public function getCacheMaxAge()
    {
        return 0;
    }
}

