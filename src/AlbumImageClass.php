<?php

/**
 * @file
 * Contains \Drupal\album\AlbumImageClass
 */

namespace Drupal\album;

use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\FileInterface;
use Drupal\file\FileStorageInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\album\AlbumFilesClass;
use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\Core\Render\Markup;
use Drupal\Core\Link;
use Symfony\Component\HttpFoundation\RedirectResponse;


class AlbumImageClass{

  public function MoveImagesUpload($id){
    
    $thumbsize = \Drupal::config('album.settings')->get('album_photo_thumb_size');
    $largesize = \Drupal::config('album.settings')->get('album_photo_large_size');

    $pattern =(new AlbumImageClass)->album_photo_pattern();
    $row = \Drupal::database()->query("select album from album where id= :albumid", [ ':albumid' => $id, ])->fetchObject();
    $album=(new AlbumImageClass)->album_get_path($id);
//    $uribase='public://'.\Drupal::config('album.settings')->get('album_directory').'/'.$row->album;
    $uribase=$album['path'];
    try{
	$files=\Drupal::service('file_system')->scanDirectory($album['path']."/upload", $pattern, [ 'recurse' => FALSE ]); 
      	foreach ($files as $file) {
  		$urisrc=$uribase.'/upload/'.$file->filename;
  		$urithumb=$uribase.'/thumbs/'.$file->filename;
  		$urilarge=$uribase.'/large/'.$file->filename;
	    		\Drupal::messenger()->addMessage($urisrc);
  		$copy=\Drupal::service('file_system')->copy($urisrc, $urithumb, FileSystemInterface::EXISTS_REPLACE); 
  		$copy=\Drupal::service('file_system')->copy($urisrc, $urilarge, FileSystemInterface::EXISTS_REPLACE); 
    		$res = \Drupal::service('image.factory')->get($urithumb);
       		if ($res) {
              		$res->scale($thumbsize);
              		$res->save();
    		}else{
	    		\Drupal::messenger()->addMessage($file->filename. 'not found');
       		}
    		//large image
    		$res = \Drupal::service('image.factory')->get($urilarge);
       		if ($res) {
	  		$res->scale($largesize);
       	  		$res->save();
    		}else{
	    		\Drupal::messenger()->addMessage($file->filename. 'not found');
       		}
  		$filedelete=new AlbumFilesClass;
    		$filedelete->handleFileDelete($urisrc);
	}
    }
    catch (Exception $e) {
		  \Drupal::messenger()->addMessage(t('no files found in upload'));
          }

  }
public function album_photo_pattern(){
        return ("/(\.jpg|\.jpeg|\.png|\.gif|\.bmp|\.JPG|\.JPEG|\.PNG|\.GIF|\.BMP)/");
}
public function album_video_pattern(){
        return ("/(\.mp4|\.ogv|\.webm|\.gif)/");
}

public function album_get_baseuri($id){
	$uri="/".PublicStream::basePath().'/'.\Drupal::config('album.settings')->get('album_directory')."/";
	$row=\Drupal::database()->query("select album from album where id= :album_id" ,array(':album_id' => $id))->fetchObject();
	return $uri.$row->album;
}
public function album_get_path($id){
	$albumpath=array();
	$path=realpath(DRUPAL_ROOT . '/' . PublicStream::basePath()).'/'.\Drupal::config('album.settings')->get('album_directory');
	$albumpath['basepath']=$path;
	$row=\Drupal::database()->query("select album,password from album where id= :album_id" ,array(':album_id' => $id))->fetchObject();
	$albumpath['album']=$row->album;
	$albumpath['password']=$row->password;
	$albumpath['path'].=$path.'/'.$albumpath['album'];
        $albumpath['htaccess_file']=$albumpath['path'].'/.htaccess';
        $albumpath['htpasswd_file']=$albumpath['path'].'/.htpasswd';
	return $albumpath;
}
public function album_get_thumb($albumid,$image){
	$row=\Drupal::database()->query("select album,password from album where id= :album_id" ,array(':album_id' => $albumid))->fetchObject();
	$url=PublicStream::basePath().'/'.\Drupal::config('album.settings')->get('album_directory');
	$thumburl=$url."/".$row->album."/thumbs/".$image;
	$thumb='<img src="/'.$thumburl.'" alt="'.$image.'">';
	return $thumb;
}
public function album_get_large($albumid,$image){
	$row=\Drupal::database()->query("select album,password from album where id= :album_id" ,array(':album_id' => $albumid))->fetchObject();
	$url=PublicStream::basePath().'/'.\Drupal::config('album.settings')->get('album_directory');
	$largeurl="/".$url."/".$row->album."/large/".$image;
	return $largeurl;
}
public function album_image_link($link,$img,$options=''){
//	$rendered_image = render($img);
	if (is_array($img)) {
  		$rendered_image = \Drupal::service('renderer')->render($img);
	}
		else {
  		$rendered_image = $img;
	}
	$image_markup = Markup::create($rendered_image);
	if(!empty($options)){
		$out= Link::fromTextAndUrl($image_markup, Url::fromUri('internal:'.$link,$options))->toString();
	}else{
		$out= Link::fromTextAndUrl($image_markup, Url::fromUri('internal:'.$link))->toString();
	}
	return $out;
}
public function album_link($link,$text,$options=''){
	if(!empty($options)){
		$out= Link::fromTextAndUrl($text, Url::fromUri('internal:'.$link,$options))->toString();
	}else{
		$out= Link::fromTextAndUrl($text, Url::fromUri('internal:'.$link))->toString();
	}
	return $out;
}
}

?>
