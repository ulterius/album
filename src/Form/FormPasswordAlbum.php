<?php

/**
 * @file
 * Contains \Drupal\album\Form\FormPasswordAlbum.
 */

namespace Drupal\album\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Component\FileSecurity\FileSecurity;
use Drupal\album\AlbumImageClass;

class FormPasswordAlbum extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form_password_album';
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state, $id = NULL) {
    $form['id'] = ['#type' => 'value', '#value' => $id];
    $form['password'] = [
      '#title' => t('Choose password'),
      '#type' => 'password_confirm',
      '#size' => 25,
    ];
    $row = \Drupal::database()->query("select password from album where id=:id", [ ':id' => $id])->fetchObject();
    if (!empty($row->password)){
    	$form['delete'] = [
      	'#title' => t('Delete password'),
      	'#type' => 'checkbox',
    	];
    }
    $form['submit'] = ['#type' => 'submit', '#value' => t('Submit')];
    return $form;
  }

  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
   $url = \Drupal\Core\Url::fromRoute('album.showalbum') ->setRouteParameters(array('albumid'=>$form_state->getValue(['id'])));
   $form_state->setRedirectUrl($url);
   $albumpath = (new AlbumImageClass)->album_get_path($form_state->getValue(['id']));
    if(!empty($form_state->getValue(['delete']))){
    	$entry['password'] = '';
      	try {
        	$count = \Drupal::database()->update('album')
          	->fields($entry)
          	->condition('id', $form_state->getValue(['id']))
          	->execute();
      	}
       	catch (Exception $e) {
        	\Drupal::messenger()->addMessage(t('\Drupal::database()->update failed. Message = %message, query= %query', [
          	'%message' => $e->getMessage(),
          	'%query' => $e->query_string,
        	]), 'error');
      	}
      	\Drupal::service("file_system")->delete($albumpath['htaccess_file']);
      	\Drupal::service("file_system")->delete($albumpath['htpasswd_file']);

    }else{
    	if ($form_state->getValue(['password'])) {
    		$password = crypt($form_state->getValue(['password']), base64_encode($form_state->getValue(['password'])));
		
    		$entry['password'] = $password;
    		$id = $form_state->getValue(['id']);
    		try {
      			$count = \Drupal::database()->update('album')
        		->fields($entry)
        		->condition('id', $id)
        		->execute();
    		}
    	
      		catch (Exception $e) {
      			\Drupal::messenger()->addMessage(t('\Drupal::database()->update failed. Message = %message, query= %query', [
        		'%message' => $e->getMessage(),
        		'%query' => $e->query_string,
      			]), 'error');
    		}
  //   		\Drupal::service("file_system")->delete($albumpath['htaccess_file']);
  //   		\Drupal::service("file_system")->delete($albumpath['htpasswd_file']);
      		$replace="FileSystemInterface::EXISTS_REPLACE";
      		$htaccess_lines = "AuthType Basic\n";
      		$htaccess_lines .= "AuthName \"". t("Authorized access : @login and chosen password", array('@login' => $albumpath['album']))."\"\n";
      		$htaccess_lines .= "AuthUserFile " . $albumpath['htpasswd_file'] . "\n";
      		$htaccess_lines .= "Require valid-user\n";
      		$htaccess_passwd = $albumpath['album'] . ':' . $password;
      		if (\Drupal::service('file_system')->saveData($htaccess_lines, $albumpath['htaccess_file'], $replace)){
        		\Drupal::service("file_system")->chmod($albumpath['htaccess_file'], 0644);
      		}
      		if (\Drupal::service('file_system')->saveData($htaccess_passwd, $albumpath['htpasswd_file'], $replace)){
        		\Drupal::service("file_system")->chmod($albumpath['htpasswd_file'], 0644);
      		}
    	}
    }
  }

}
?>
