<?php

/**
 * @file
 * Contains \Drupal\album\Form\FormImbeddedVideo.
 */

namespace Drupal\album\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\album\AlbumImageClass;

class FormImbeddedVideo extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form_album_video';
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state, $albumid = NULL, $id = NULL) {
    $form = [];
    if (!empty($id)) {
      $con = \Drupal\Core\Database\Database::getConnection();
      $query = $con->select('albumembeddedvideo', 'p');
      $query->addField('p', 'videoid');
      $query->addField('p', 'title');
      $query->addField('p', 'description');
      $query->addField('p', 'albumid');
      $query = $query->condition('p.id', $id);
      $row = $query->execute()->fetchAssoc();
      $form['id'] = ['#type' => 'value', '#value' => $id];
      $form['albumid'] = ['#type' => 'value', '#value' => $row['albumid']];
    }
    else {
      $form['albumid'] = ['#type' => 'value', '#value' => $albumid];
    }


    $form['videoid'] = [
      '#type' => 'textfield',
      '#title' => t('Album id Youtube'),
      '#description' => t('for example: -STmC8GJGe8'),
      '#default_value' => !empty($row['videoid']) ? $row['videoid'] : '',
    ];
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => t('Title video'),
      '#default_value' => !empty($row['title']) ? $row['title'] : '',
      '#size' => 30,
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => t('Description video'),
      '#cols' => 50,
      '#rows' => 5,
      '#default_value' => !empty($row['description']) ? $row['description'] : '',
    ];

    $form['submit'] = ['#type' => 'submit', '#value' => t('Submit')];
    return $form;
  }

  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $url = \Drupal\Core\Url::fromRoute('album.showalbum') ->setRouteParameters(array('albumid'=>$form_state->getValue(['albumid'])));
    $form_state->setRedirectUrl($url);

    $id = $form_state->getValue(['id']);
    $entry['videoid'] = $form_state->getValue(['videoid']);
    $entry['albumid'] = $form_state->getValue(['albumid']);
    $entry['title'] = $form_state->getValue(['title']);
    $entry['description'] = $form_state->getValue(['description']);
    if ($id) {
      try {
        $return_value = \Drupal::database()->update('albumembeddedvideo')
          ->fields($entry)
          ->condition('id', $id)
          ->execute();
      }
      
        catch (Exception $e) {
        \Drupal::messenger()->addMessage(t('\Drupal::database()->update failed. Message = %message, query= %query', [
          '%message' => $e->getMessage(),
          '%query' => $e->query_string,
        ]), 'error');
      }

    }
    else {
      try {
        $return_value = \Drupal::database()->insert('albumembeddedvideo')
          ->fields($entry)
          ->execute();
      }
      
        catch (Exception $e) {
        \Drupal::messenger()->addMessage(t('\Drupal::database()->insert failed. Message = %message, query= %query', [
          '%message' => $e->getMessage(),
          '%query' => $e->query_string,
        ]), 'error');
      }

    }
  }

}
?>
