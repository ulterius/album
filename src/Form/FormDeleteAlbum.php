<?php

/**
 * @file
 * Contains \Drupal\album\Form\FormDeleteAlbum.
 */

namespace Drupal\album\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\album\AlbumImageClass;

class FormDeleteAlbum extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form_delete_album';
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state, $id = NULL) {
    $albumpath = (new AlbumImageClass)->album_get_path($id);
    $form['id'] = ['#type' => 'value', '#value' => $id];
    $form['album'] = ['#type' => 'value', '#value' => $albumpath['album']];
    $form['submit'] = [
      '#prefix' => $albumpath['album'] . ': ',
      '#type' => 'submit',
      '#value' => t('Delete this album including all photo\'s'),
    ];
    return $form;


  }

  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $form_state->setRedirect('album.album');
    $id = $form_state->getValue(['id']);
    $albumpath = (new AlbumImageClass)->album_get_path($id);
	\Drupal::service('file_system')->deleteRecursive($albumpath['path']);
	try {
        	$query = \Drupal::database()->delete('albumphoto')
        	->condition('albumid', $id)
        	->execute();
    	}
      catch (Exception $e) {
      		\Drupal::messenger()->addMessage(t('db_delete failed. Message = %message, query= %query', [
        	'%message' => $e->getMessage(),
        	'%query' => $e->query_string,
      		]), 'error');
    	}

	try {
        	$query = \Drupal::database()->delete('album')
        	->condition('id', $id)
        	->execute();
    	}
      catch (Exception $e) {
      		\Drupal::messenger()->addMessage(t('db_delete failed. Message = %message, query= %query', [
        	'%message' => $e->getMessage(),
        	'%query' => $e->query_string,
      		]), 'error');
    	}

    if (!empty($error)) {
      \Drupal::messenger()->addMessage($msg, $error);
    }
    else {
      \Drupal::messenger()->addMessage($form_state->getValue(['album']) . t('is deleted'));
    }
  }

}
?>
