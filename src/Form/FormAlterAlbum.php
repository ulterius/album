<?php

/**
 * @file
 * Contains \Drupal\album\Form\FormAlterAlbum.
 */

namespace Drupal\album\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\album\AlbumImageClass;
use Drupal\album\AlbumGallery;

class FormAlterAlbum extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form_delete_album';
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state, $id = NULL) {
        $row=\Drupal::database()->query("select album,title from album where id= :album_id" ,array(':album_id' => $id))->fetchObject();
        $album=$row->album;
        $form['id'] = array('#type' => 'value', '#value' => $id);
        $form['newalbum'] = array(
        '#type' => 'textfield',
        '#title' => t('Rename this album'),
        '#size' => 40,
        '#default_value' => $album,
        );
        $form['title'] = array(
        '#type' => 'textfield',
        '#title' => t('Title'),
        '#size' => 40,
        '#default_value' => !empty($row->title) ? $row->title : '',
        );
        $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));
        return $form;
  }

  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
	$url = \Drupal\Core\Url::fromRoute('album.showalbum') ->setRouteParameters(array('albumid'=>$form_state->getValue(['id'])));
        $form_state->setRedirectUrl($url);
    	$id = $form_state->getValue(['id']);
        $albumpath=(new AlbumImageClass)->album_get_path($id);
    	$newalbum = $form_state->getValue(['newalbum']);
        $newalbum=(new AlbumGallery)->sanitize_albumname($newalbum);
        if( $albumpath['album'] != $newalbum){
                unset($error);
                if (!rename($albumpath['path'],$albumpath['basepath']."/".$newalbum)){
                        \Drupal::messenger()->addMessage( 'Album '.$albumpath['album']. t(' cannot be renamed'),$error);
                }else{
                        \Drupal::messenger()->addMessage( 'Album  '.$albumpath['album']. t(' is renamed to '). $newalbum );
                }
        }
        $entry['album']=$newalbum;
    	$entry['title'] = $form_state->getValue(['title']);
        try {
                    $count = \Drupal::database()->update('album')
                    ->fields($entry)
                    ->condition('id', $id)
                   ->execute();
            }
                   catch (Exception $e) {
                   \Drupal::messenger()->addMessage(t('\Drupal::database()->update failed. Message = %message, query= %query',
                  array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
              }
}
}
?>
