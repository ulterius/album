<?php

/**
 * @file
 * Contains \Drupal\album\Form\FormDeleteAlbumVideo.
 */

namespace Drupal\album\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\album\AlbumImageClass;

class FormDeleteAlbumVideo extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form_delete_album_video';
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state, $id = NULL) {
    $row = \Drupal::database()->query("select video,albumid from albumvideo where id=:id", [
      ':id' => $id
      ])->fetchObject();
    $albumpath = (new AlbumImageClass)->album_get_path($row->albumid);
    $form['albumid'] = ['#type' => 'value', '#value' => $row->albumid];
    $form['id'] = ['#type' => 'value', '#value' => $id];
    $form['video'] = ['#type' => 'value', '#value' => $row->video];
    $form['submit'] = [
      '#prefix' => $row->video,
      '#type' => 'submit',
      '#value' => t('Delete this video'),
    ];
    return $form;
  }

  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $url = \Drupal\Core\Url::fromRoute('album.showalbum') ->setRouteParameters(array('albumid'=>$form_state->getValue(['albumid'])));
    $form_state->setRedirectUrl($url);
    $id = $form_state->getValue(['id']);
    $video = $form_state->getValue(['video']);
    $albumpath = $form_state->getValue(['albumpath']);
    try {
        $query = \Drupal::database()->delete('albumvideo')
        ->condition('id', $id)
        ->execute();
    }

      catch (Exception $e) {
      \Drupal::messenger()->addMessage(t('db_delete failed. Message = %message, query= %query', [
        '%message' => $e->getMessage(),
        '%query' => $e->query_string,
      ]), 'error');
    }
    if(\Drupal::service('file_system')->delete($albumpath . "/" . $video)){
      \Drupal::messenger()->addMessage(t('Video is deleted'));
    }
    else {
      \Drupal::messenger()->addMessage(t('Problem removing photo') . $albumpath . " " . $video, $error = 'error');
    }
  }
}
?>
