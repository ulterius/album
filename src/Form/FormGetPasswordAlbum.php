<?php

/**
 * @file
 * Contains \Drupal\album\Form\FormGetPasswordAlbum.
 */

namespace Drupal\album\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Component\FileSecurity\FileSecurity;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Drupal\album\AlbumImageClass;

class FormGetPasswordAlbum extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form_password_album';
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state, $id = NULL) {
    $albumpath = (new AlbumImageClass)->album_get_path($id);
    $form['id'] = ['#type' => 'value', '#value' => $id];
    $form['password'] = [
	'#title' => t('Password for: ').$albumpath['album'],
      '#type' => 'password',
      '#size' => 25,
    ];
    $form['submit'] = ['#type' => 'submit', '#value' => t('Submit')];
    return $form;
  }

  public function validateForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
	$password = crypt($form_state->getValue(['password']), base64_encode($form_state->getValue(['password'])));
	  $row = \Drupal::database()->query("select password from album where id=:id", [ ':id' => $form_state->getValue(['id']) ])->fetchObject();
	  if ($row->password != $password){
		  $form_state->setErrorByName('password', $this->t('Invalid password').$cookie);
	  }
  }

  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
	  $url = \Drupal\Core\Url::fromRoute('album.showalbum') ->setRouteParameters(array('albumid'=>$form_state->getValue(['id'])));
    $form_state->setRedirectUrl($url);
	$cookievalue=crypt($form_state->getValue(['password']), base64_encode($form_state->getValue(['password'])));
	$cookiename='album'.$form_state->getValue(['id']);
	$cookielifetime=(86400 * 30);
	$cookiepath=(new AlbumImageClass)->album_get_baseuri($form_state->getValue(['id']));
	user_cookie_save([$cookiename => $cookievalue]);
  }

}
?>
