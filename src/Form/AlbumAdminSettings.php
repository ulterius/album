<?php

/**
 * @file
 * Contains \Drupal\album\Form\AlbumAdminSettings.
 */

namespace Drupal\album\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\album\AlbumImageClass;

class AlbumAdminSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'album_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('album.settings');

    foreach (Element::children($form) as $variable) {
      $config->set($variable, $form_state->getValue($form[$variable]['#parents']));
    }
    $config->save();

    if (method_exists($this, '_submitForm')) {
      $this->_submitForm($form, $form_state);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['album.settings'];
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $form = [];
    $form['album_directory'] = [
      '#type' => 'textfield',
      '#title' => t('Album directory'),
      '#description' => t('Directory where the photos are stored'),
      '#default_value' => \Drupal::config('album.settings')->get('album_directory'),
    ];
    $form['album_photo_large_size'] = [
      '#type' => 'textfield',
      '#title' => t('Size of uploaded large images '),
      '#description' => t('Horizontal size, vertical size will be automatically  adjusted'),
      '#default_value' => \Drupal::config('album.settings')->get('album_photo_large_size'),
    ];
    $form['album_photo_thumb_size'] = [
      '#type' => 'textfield',
      '#title' => t('Size of uploaded thumbnail images '),
      '#default_value' => \Drupal::config('album.settings')->get('album_photo_thumb_size'),
    ];
    $form['album_video_size'] = [
      '#type' => 'textfield',
      '#title' => t('Video size'),
      '#default_value' => \Drupal::config('album.settings')->get('album_video_size'),
    ];
    $form['album_youtube_url'] = [
      '#type' => 'textfield',
      '#title' => t('Youtube imbedded url'),
      '#maxlength' => 255,
      '#default_value' => \Drupal::config('album.settings')->get('album_youtube_url'),
    ];
    $form['album_autoscan'] = [
      '#type' => 'checkbox',
      '#title' => t('Auto scan galleries'),
      '#default_value' => \Drupal::config('album.settings')->get('album_autoscan'),
    ];
    return parent::buildForm($form, $form_state);
  }

}
?>
