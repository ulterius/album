<?php

/**
 * @file
 * Contains \Drupal\album\Form\FormUploadAlbumVideo.
 */

namespace Drupal\album\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\file\Entity\File;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\FileInterface;
use Drupal\file\FileStorageInterface;

use Drupal\album\AlbumImageClass;

class FormUploadAlbumVideo extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form_upload_album_video';
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state, $id = NULL) {
    $form = [];
    $form['id'] = ['#type' => 'value', '#value' => $id];
    $row = \Drupal::database()->query("select album from album where id=:album_id", [ ':album_id' => $id ])->fetchObject();
    $album = $row->album;
    $form['album'] = ['#type' => 'value', '#value' => $album];
    $form['text1'] = [
      '#type' => 'item',
      '#markup' => '<h1>' . t('Upload 1 or more video to ') . $album . '</h1>',
    ];
    $form['videos'] = [
      '#title' => t('Videos'),
      '#type' => 'managed_file',
      '#required' => FALSE,
      '#upload_location' => 'public://'.\Drupal::config('album.settings')->get('album_directory').'/'.$album,
      '#multiple' => TRUE,
      '#upload_validators' => [
        'file_validate_extensions' => $this->getAllowedFileExtensions(),
      ],
    ];

    $form['submit_upload'] = [
      '#type' => 'submit',
      '#value' => t('Submit'),
    ];
    return $form;

  }

  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
	$url = \Drupal\Core\Url::fromRoute('album.showalbum') ->setRouteParameters(array('albumid'=>$form_state->getValue(['id'])));
    $form_state->setRedirectUrl($url);
	$id = $form_state->getValue(['id']);
    $album = $form_state->getValue(['album']);
    $albumpath = (new AlbumImageClass)->album_get_path($id);
    $filenames = array();
    // Loop through uploaded files.
    $files = $form_state->getValue('videos');
    foreach ($files as $fid) {
      $file = File::load($fid);
      $file->setPermanent();
      $file->save();
      $name = $file->getFilename();
      $filenames [] = $name;
    }
    $files_count = count($filenames);
    // If there is 1 file uploaded.
    $filename =  ($files_count == 1) ? $filenames[0] : '';
    // If there are multiple files uploaded
    $filenames_imploded = ($files_count > 1) ? implode(', ', $filenames) : '';
    // Build message, based on file count.
    if ($filename){
      $message = t('One file uploaded successfully: ') .$filename;
    } elseif ($filenames_imploded) {
      $message = t('@files_count files uploaded successfully: ',
          array('@files_count' => $files_count)) .$filenames_imploded;
    }

    foreach ($filenames as $filename){
    	$entry['video']=$filename;
    	$entry['title'] =$filename;
    	try {
            	$id = \Drupal::database()->insert('albumvideo')
                  	->fields($entry)
                  	->execute();
        	}
    	catch (Exception $e) {
            	\Drupal::messenger()->addMessage(t('\Drupal::database()->insert failed. Message = %message, query= %query',
             	array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
    	}
    }
  }
  private function getAllowedFileExtensions(){
    return array('mp4 ogv webm gif');
  }

}
?>
