<?php

/**
 * @file
 * Contains \Drupal\album\Form\FormAdminAlbum.
 */

namespace Drupal\album\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Routing;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\album\AlbumImageClass;


class FormAdminAlbum extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form_admin_album';
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state, $id = NULL) {
        $form['id'] = array('#type' => 'value', '#value' => $id);
        $adminselect=array(
                        '' => '',
                        'albumedit' => t('Edit album name'),
			//Cache problems with images....
                        'password' => t('Set password'),
                        'addphotos' => t('Upload photos'),
                        'uploadvideo' => t('Upload video'),
                        'addvideo' => t('Add Youtube video'),
                        'albumlist' => t('Album admin list'),
                        'deletealbum' => t('Delete album'),
                );
         $form['admin'] = array(
                  '#description' => t('Admin'),
                  '#type' => 'select',
                  '#default' => '',
                   '#options' => $adminselect,
                 '#attributes' => array('onchange' => 'this.form.submit()'),
                 );
        $form['submit'] = array( '#type' => 'submit', '#value' => 'Doe', '#attributes' => array('style' => "display:none;"),);
        return $form;
  }

  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $id = $form_state->getValue(['id']);
    $admin = $form_state->getValue(['admin']);
    switch ($admin) {
                case 'albumedit':
                        $goto='/album/alteralbum/'.$id;
                break;
                case 'password':
                        $goto='/album/password/'.$id;
                break;
                case 'addphotos':
                        $goto='/album/upload/'.$id;
                break;
                case 'uploadvideo':
                        $goto='/album/upload-video/'.$id;
                break;
                case 'addvideo':
                        $goto='/album/imbedded-video/'.$id;
                break;
                case 'albumlist':
                        $goto='/album/list/'.$id;
                break;
                case 'deletealbum':
                        $goto='/album/deletealbum/'.$id;
                break;
                default:
                        $goto='/album/show/'.$id;
        }
	$url = Url::fromUri('internal:' . $goto);
	$form_state->setRedirectUrl( $url );

	}
}
?>
