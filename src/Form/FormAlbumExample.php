<?php
namespace Drupal\album\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\album\AlbumImageClass;

class FormAlbumExample extends FormBase {
public function getFormId() {
    return 'example_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
    );

    $form['phone'] = array(
      '#type' => 'tel',
      '#title' => $this->t('Your phone number'),
    );

    // Define the buttons.
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    );

    return $form;
  }
  public function validateForm(array &$form, FormStateInterface $form_state) {   
    $form_state->setErrorByName('phone_number', $this->t('Error with phone number'));
  }
  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal::messenger()->addMessage($this->t("Hi %name, your phone number is %phone.", array(
      '%name' => $form_state->getValue('name'),
      '%phone' => $form_state->getValue('phone'),
    )));
  }
}
