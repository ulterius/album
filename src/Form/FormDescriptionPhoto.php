<?php

/**
 * @file
 * Contains \Drupal\album\Form\FormDescriptionPhoto.
 */

namespace Drupal\album\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Database\Connection;
use Drupal\album\AlbumImageClass;

class FormDescriptionPhoto extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form_description_photo';
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state, $id = NULL) {
    //	$row=\Drupal::database()->query("select * from album where album=:album and photo=:photo",array(':album' => $album,':photo' => $photo))->fetchObject();
    $con = \Drupal\Core\Database\Database::getConnection();
    $query = $con->select('albumphoto', 'p');
    $query->addField('p', 'id');
    $query->addField('p', 'photo');
    $query->addField('p', 'albumphoto');
    $query->addField('p', 'random');
    $query->addField('p', 'title');
    $query->addField('p', 'description');
    $query->addField('p', 'albumid');
    $query = $query->condition('p.id', $id);
    $row = $query->execute()->fetchAssoc();

    $albumpath = (new AlbumImageClass)->album_get_baseuri($row['albumid']);
    $photo = "<img src=\"" . $albumpath. "/thumbs/" . $row['photo'] . "\"/>";
    $form = [];
    $form['picture'] = [
      '#type' => 'item',
      '#markup' => '<h1>' . $row['photo'] . '</h1>' . $photo,
    ];

    $form['id'] = ['#type' => 'value', '#value' => $id];
    $form['albumid'] = ['#type' => 'value', '#value' => $row['albumid']];
    $form['path'] = ['#type' => 'value', '#value' => $albumpath];
    $form['photo'] = ['#type' => 'value', '#value' => $row['photo']];

    $form['albumphoto'] = [
      '#type' => 'checkbox',
      '#title' => t('Select as album photo'),
      '#default_value' => !empty($row['albumphoto']) ? $row['albumphoto'] : '0',
    ];
    $form['random'] = [
      '#type' => 'checkbox',
      '#title' => t('Select photo for random thumbnail gallery'),
      '#default_value' => !empty($row['random']) ? $row['random'] : '0',
    ];
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => t('Title photo'),
      '#default_value' => !empty($row['title']) ? $row['title'] : '',
      '#size' => 30,
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => t('Description photo'),
      '#cols' => 50,
      '#rows' => 5,
      '#default_value' => !empty($row['description']) ? $row['description'] : '',
    ];

    $form['submit'] = ['#type' => 'submit', '#value' => t('Submit')];
    return $form;
  }

  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $url = \Drupal\Core\Url::fromRoute('album.showalbum') ->setRouteParameters(array('albumid'=>$form_state->getValue(['albumid'])));
    $form_state->setRedirectUrl($url);
    $albumid = $form_state->getValue(['albumid']);
    $path = $form_state->getValue(['path']);
    $photo = $form_state->getValue(['photo']);
    $title = $form_state->getValue(['title']);
    $description = $form_state->getValue(['description']);
    $albumphoto = $form_state->getValue(['albumphoto']);
    $random = $form_state->getValue(['random']);
    $entry['albumphoto'] = $albumphoto;
    $entry['random'] = $random;
    $entry['title'] = $title;
    $entry['description'] = $description;
    $entry['albumid'] = $albumid;
    $entry['photo'] = $photo;
    $con = \Drupal\Core\Database\Database::getConnection();
    $query = $con->select('albumphoto', 'p');
    $query->addField('p', 'id');
    $query = $query ->condition('p.albumid', $albumid);
    $query = $query ->condition('p.photo', $photo);
    $row = $query->execute()->fetchAssoc();
    if ($row) {
      $id = $row['id'];
      try {
        $count = \Drupal::database()->update('albumphoto')
          ->fields($entry)
          ->condition('id', $id)
          ->execute();
      }
      
        catch (Exception $e) {
        \Drupal::messenger()->addMessage(t('\Drupal::database()->update failed. Message = %message, query= %query', [
          '%message' => $e->getMessage(),
          '%query' => $e->query_string,
        ]), 'error');
      }

    }
    else {
      try {
        $return_value = \Drupal::database()->insert('albumphoto')
          ->fields($entry)
          ->execute();
      }
      
        catch (Exception $e) {
        \Drupal::messenger()->addMessage(t('\Drupal::database()->insert failed. Message = %message, query= %query', [
          '%message' => $e->getMessage(),
          '%query' => $e->query_string,
        ]), 'error');
      }

    }
  }

}
?>
