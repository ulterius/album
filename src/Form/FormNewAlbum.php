<?php

/**
 * @file
 * Contains \Drupal\album\Form\FormNewAlbum.
 */

namespace Drupal\album\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\album\AlbumImageClass;
use Drupal\album\AlbumGallery;

class FormNewAlbum extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form_new_album';
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state,$path = NULL) {
	if(!empty($id)){
                $form['id'] = array('#type' => 'value', '#value' => $id);
        }
        $form['path'] = array('#type' => 'value', '#value' => $path);
        $form['album'] = array(
        '#type' => 'textfield',
        '#title' => t('Create a new album'),
        '#size' => 40,
        );
        $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));
        return $form;

  }
  public function validateForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
        $album=$form_state->getValue(['album']);
	$album=(new AlbumGallery)->sanitize_albumname($album);

	 if(\Drupal::database()->query("select album from album where album= :album" ,array(':album' => $album))->fetchObject()){
                  $form_state->setErrorByName('album', t('Allready exists!'));
          }
  }


  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $id = $form_state->getValue(['id']);
	$path=$form_state->getValue(['path']);
        $album=$form_state->getValue(['album']);
        $album=(new AlbumGallery)->sanitize_albumname($album);
        if(! mkdir($path.'/'.$album."/thumbs/", 0755,true)){
                \Drupal::messenger()->addMessage( $path.'/'.$album."/thumbs/".t(' doesn\'t exists or cannot be created!'),$error);
        }
        if(! mkdir($path.'/'.$album."/large/", 0755,true)){
                \Drupal::messenger()->addMessage( $path.'/'.$album."/large/".t(' doesn\'t exists or cannot be created!'),$error);
        }
        if(! mkdir($path.'/'.$album."/upload/", 0755,true)){
                \Drupal::messenger()->addMessage( $path.'/'.$album."/upload/".t(' doesn\'t exists or cannot be created!'),$error);
        }
        $entry['album']=$album;
        $entry['title']=$album;
        try {
                $id = \Drupal::database()->insert('album')
                ->fields($entry)
                ->execute();
                }
                catch (Exception $e) {
                        \Drupal::messenger()->addMessage(t('\Drupal::database()->insert failed. Message = %message, query= %query',
                        array('%message' => $e->getMessage(), '%query' => $e->query_string)), 'error');
                }
  //      drupal_goto('/album/show/' . $id);
}
}
?>
