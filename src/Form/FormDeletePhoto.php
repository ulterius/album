<?php

/**
 * @file
 * Contains \Drupal\album\Form\FormDeletePhoto.
 */

namespace Drupal\album\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\FileInterface;
use Drupal\file\FileStorageInterface;
use Drupal\album\AlbumImageClass;


class FormDeletePhoto extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form_delete_photo';
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state, $id = NULL) {
    $row = \Drupal::database()->query("select photo,albumid from albumphoto where id=:id", [ ':id' => $id ])->fetchObject();
    $albumuri = (new AlbumImageClass)->album_get_baseuri($row->albumid);
    $albumpath = (new AlbumImageClass)->album_get_path($row->albumid);

    $form['albumpath'] = ['#type' => 'value', '#value' => $albumpath['path']];
    $form['id'] = ['#type' => 'value', '#value' => $id];
    $form['albumid'] = ['#type' => 'value', '#value' => $row->albumid];
    $form['photo'] = ['#type' => 'value', '#value' => $row->photo];
    $form['submit'] = [
      '#prefix' => '<img src=' . $albumuri . '/thumbs/' . $row->photo . '><p>',
      '#type' => 'submit',
      '#value' => t('Delete this photo'),
    ];
    return $form;
  }

  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $url = \Drupal\Core\Url::fromRoute('album.showalbum') ->setRouteParameters(array('albumid'=>$form_state->getValue(['albumid'])));
    $form_state->setRedirectUrl($url);

    $id = $form_state->getValue(['id']);
    $photo = $form_state->getValue(['photo']);
    $albumpath = $form_state->getValue(['albumpath']);
    try {
	$query = \Drupal::database()->delete('albumphoto')
        ->condition('id', $id)
        ->execute();
    }
    
      catch (Exception $e) {
      \Drupal::messenger()->addMessage(t('db_delete failed. Message = %message, query= %query', [
        '%message' => $e->getMessage(),
        '%query' => $e->query_string,
      ]), 'error');
    }
    if(\Drupal::service('file_system')->delete($albumpath . "/large/" . $photo) && \Drupal::service('file_system')->delete($albumpath . "/thumbs/" . $photo)){
      \Drupal::messenger()->addMessage(t('Photo is deleted'));
    }
    else {
      \Drupal::messenger()->addMessage(t('Problem removing photo') . $albumpath . " " . $photo, $error = 'error');
    }
  }

}
?>
