<?php

/**
 * @file
 * Contains \Drupal\album\Form\FormDeleteImbeddedVideo.
 */

namespace Drupal\album\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\album\AlbumImageClass;

class FormDeleteImbeddedVideo extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form_delete_imbedded_video';
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state, $id = NULL) {
    $row = \Drupal::database()->query("select videoid,albumid from albumembeddedvideo where id=:id ", [
      'id' => $id
      ])->fetchObject();
    $form = [];
    $form['id'] = ['#type' => 'value', '#value' => $id];
    $form['albumid'] = ['#type' => 'value', '#value' => $row->albumid];
    $form['submit'] = [
      '#prefix' => $row->videoid . ' ',
      '#type' => 'submit',
      '#value' => t('Delete this video from this site, not Youtube'),
    ];
    return $form;
  }

  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $url = \Drupal\Core\Url::fromRoute('album.showalbum') ->setRouteParameters(array('albumid'=>$form_state->getValue(['albumid'])));
    $form_state->setRedirectUrl($url);
    $id = $form_state->getValue(['id']);
    try {
      $query = \Drupal::database()->delete('albumembeddedvideo')
        ->condition('id', $id)
        ->execute();
    }
    
      catch (Exception $e) {
      \Drupal::messenger()->addMessage(t('db_delete failed. Message = %message, query= %query', [
        '%message' => $e->getMessage(),
        '%query' => $e->query_string,
      ]), 'error');
    }

  }

}
?>
