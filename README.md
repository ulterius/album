A Drupal 9/10 photo gallery module  with colorbox.

This software has travelled a long way from standalone php website to Drupal 6 -> 7 -> now 10.


This module can:

- Upload unmanaged photo's/images and video's or scan existing.
- Show photo's with the colorbox module.
- html5 video's
- Embedded youtube video's

